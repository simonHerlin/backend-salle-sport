from django.apps import AppConfig


class GestionSalleConfig(AppConfig):
    name = 'gestion_salle'
