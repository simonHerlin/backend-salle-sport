# Generated by Django 3.0.3 on 2020-05-07 18:44

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestion_salle', '0007_auto_20200507_0727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='date_of_birth',
            field=models.DateField(verbose_name=datetime.datetime(2020, 5, 7, 18, 44, 27, 751245)),
        ),
        migrations.AlterField(
            model_name='musclesize',
            name='date',
            field=models.DateField(verbose_name=datetime.datetime(2020, 5, 7, 18, 44, 27, 778760)),
        ),
    ]
