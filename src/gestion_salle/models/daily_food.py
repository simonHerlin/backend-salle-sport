from django.db import models
import datetime


class DailyFood(models.Model):
    protein = models.FloatField(default=0)
    lipid = models.FloatField(default=0)
    carbohydrate = models.FloatField(default=0)
    date = models.DateField(datetime.datetime.now())
