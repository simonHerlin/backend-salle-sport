from django.db import models


class PostalCode(models.Model):
    postal_code = models.CharField(max_length=10)

    def __str__(self):
        return self.postal_code
