from django.db import models
from rest_framework_simplejwt.state import User


class Performance(models.Model):
    client = models.ForeignKey(
        User,
        to_field='id',
        on_delete=models.CASCADE
    )
    equipment = models.ForeignKey(
        'BodyBuildingEquipment',
        to_field='name',
        on_delete=models.CASCADE
    )
    score = models.IntegerField()

    def __str__(self):
        return "Client : {}, équipement : {} : {} Kg".format(self.client, self.equipment, self.score)
