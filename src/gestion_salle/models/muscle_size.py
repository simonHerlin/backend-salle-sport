from django.db import models
import datetime

from rest_framework_simplejwt.state import User


class MuscleSize(models.Model):
    ankle = models.FloatField(default=0)
    calf = models.FloatField(default=0)
    thigh = models.FloatField(default=0)
    waist = models.FloatField(default=0)
    pectoral = models.FloatField(default=0)
    shoulder = models.FloatField(default=0)
    wrist = models.FloatField(default=0)
    forearm = models.FloatField(default=0)
    arm = models.FloatField(default=0)
    neck = models.FloatField(default=0)
    weight = models.FloatField(default=0)
    body_fat_percentage = models.FloatField(default=20)
    date = models.DateField(datetime.datetime.now())
    client = models.ForeignKey(
        User,
        to_field='id',
        on_delete=models.CASCADE
    )
    goal = models.BooleanField(default=False)

    def __str__(self):
        return "{} le {}, Cheville : {}, Mollet : {}, Cuisse : {}, Taille: {}, " \
               "Pectoraux : {}, Epaule: {}, Poignet : {}, Avant-bras : {}, Bras : {}, " \
               "Cou : {}, le poid: {}, le taux de gras: {}".format(self.client, self.date, self.ankle, self.calf,
                                                                   self.thigh, self.waist, self.pectoral, self.shoulder,
                                                                   self.wrist, self.forearm, self.arm, self.neck,
                                                                   self.weight, self.body_fat_percentage)
