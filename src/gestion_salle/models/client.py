from django.contrib.auth.models import User
from django.db import models
import datetime


class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_of_birth = models.DateField(datetime.datetime.now())
    phone = models.CharField(max_length=10)
    code = models.CharField(max_length=10)
    permission = models.ForeignKey(
        'Permission',
        to_field='id',
        on_delete=models.CASCADE
    )
    city = models.ForeignKey(
        'City',
        to_field='id',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name + " " + self.user.email
    """
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    date_of_birth = models.DateField(datetime.datetime.now())
    email = models.EmailField(null=False, unique=True)
    phone = models.CharField(max_length=10)
    code = models.CharField(max_length=10)
    permission = models.ForeignKey(
        'Permission',
        to_field='id',
        on_delete=models.CASCADE
    )
    city = models.ForeignKey(
        'City',
        to_field='id',
        on_delete=models.CASCADE
    )
    users = User

    def __str__(self):
        return self.first_name + " " + self.last_name + " " + self.email
    """