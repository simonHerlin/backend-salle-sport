from django.db import models


class City(models.Model):
    name = models.CharField(max_length=50)
    postal_code = models.ForeignKey(
        'PostalCode',
        to_field='id',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name + " " + self.postal_code.postal_code
