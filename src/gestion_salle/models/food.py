from django.db import models


class Food(models.Model):
    name = models.CharField(max_length=100)
    protein = models.FloatField(default=0)
    lipid = models.FloatField(default=0)
    carbohydrate = models.FloatField(default=0)
    gram = models.FloatField(default=0)
