from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.city import City
from gestion_salle.api.serializers import CitySerializer


class CityViewSet(viewsets.ModelViewSet):
    serializer_class = CitySerializer

    queryset = City.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id', 'name', 'postal_code', ]
