from .client_viewset import ClientViewSet
from .permission_viewset import PermissionViewSet
from .muscle_size_viewset import MuscleSizeViewSet
from .bodybuilding_equipment_viewset import BodyBuildingEquipmentViewSet
from .one_muscle_size_viewset import (ForearmSizeViewSet,
                                      CalfSizeViewSet,
                                      ArmSizeViewSet,
                                      AnkleSizeViewSet,
                                      NeckSizeViewSet,
                                      PectoralSizeViewSet,
                                      ShoulderSizeViewSet,
                                      ThighSizeViewSet,
                                      WaistSizeViewSet,
                                      WeightViewSet,
                                      BodyFatViewSet,)
from .food_viewset import FoodViewSet
from .daily_food_viewset import DailyFoodViewSet

[
    ClientViewSet,
    PermissionViewSet,
    MuscleSizeViewSet,
    BodyBuildingEquipmentViewSet,
    ForearmSizeViewSet,
    CalfSizeViewSet,
    ArmSizeViewSet,
    AnkleSizeViewSet,
    NeckSizeViewSet,
    PectoralSizeViewSet,
    ShoulderSizeViewSet,
    ThighSizeViewSet,
    WaistSizeViewSet,
    WeightViewSet,
    BodyFatViewSet,
    FoodViewSet,
    DailyFoodViewSet,
]
