from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.food import Food
from gestion_salle.api.serializers import FoodSerializer


class FoodViewSet(viewsets.ModelViewSet):
    serializer_class = FoodSerializer
    queryset = Food.objects.all()

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'protein', 'lipid', 'carbohydrate']
