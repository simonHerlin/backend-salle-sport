from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.permission import Permission
from gestion_salle.api.serializers import PermissionSerializer


class PermissionViewSet(viewsets.ModelViewSet):
    serializer_class = PermissionSerializer
    queryset = Permission.objects.all()

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', ]
