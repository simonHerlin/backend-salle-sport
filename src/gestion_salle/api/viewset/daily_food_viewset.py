from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.daily_food import DailyFood
from gestion_salle.api.serializers import DailyFoodSerializer


class DailyFoodViewSet(viewsets.ModelViewSet):
    serializer_class = DailyFoodSerializer
    queryset = DailyFood.objects.all()

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['protein', 'lipid', 'carbohydrate', 'date']
