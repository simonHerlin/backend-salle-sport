from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.postal_code import PostalCode
from gestion_salle.api.serializers import PostalCodeSerializer


class PostamCodeViewSet(viewsets.ModelViewSet):
    serializer_class = PostalCodeSerializer

    queryset = PostalCode.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['postal_code', ]
