from django.db.models.functions import TruncYear
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.muscle_size import MuscleSize
from gestion_salle.api.serializers import (WristMuscleSizeSerializer,
                                           WaistMuscleSizeSerializer,
                                           ThighMuscleSizeSerializer,
                                           ShoulderMuscleSizeSerializer,
                                           ForearmMuscleSizeSerializer,
                                           NeckMuscleSizeSerializer,
                                           PectoralMuscleSizeSerializer,
                                           CalfMuscleSizeSerializer,
                                           ArmMuscleSizeSerializer,
                                           AnkleMuscleSizeSerializer,
                                           WeightMuscleSizeSerializer,
                                           BodyFatSerializer,)


class AnkleSizeViewSet(viewsets.ModelViewSet):
    serializer_class = AnkleMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['client', 'goal']


class WristSizeViewSet(viewsets.ModelViewSet):
    serializer_class = WristMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['wrist', 'date', 'client', 'goal']


class CalfSizeViewSet(viewsets.ModelViewSet):
    serializer_class = CalfMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['calf', 'date', 'client', 'goal']


class ThighSizeViewSet(viewsets.ModelViewSet):
    serializer_class = ThighMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['thigh', 'date', 'client', 'goal']


class WaistSizeViewSet(viewsets.ModelViewSet):
    serializer_class = WaistMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['waist', 'date', 'client', 'goal']


class PectoralSizeViewSet(viewsets.ModelViewSet):
    serializer_class = PectoralMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['pectoral', 'date', 'client', 'goal']


class ShoulderSizeViewSet(viewsets.ModelViewSet):
    serializer_class = ShoulderMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['shoulder', 'date', 'client', 'goal']


class ForearmSizeViewSet(viewsets.ModelViewSet):
    serializer_class = ForearmMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['forearm', 'date', 'client', 'goal']


class ArmSizeViewSet(viewsets.ModelViewSet):
    serializer_class = ArmMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['arm', 'date', 'client', 'goal']


class NeckSizeViewSet(viewsets.ModelViewSet):
    serializer_class = NeckMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['neck', 'date', 'client', 'goal']


class WeightViewSet(viewsets.ModelViewSet):
    serializer_class = WeightMuscleSizeSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['weight', 'date', 'client', 'goal']


class BodyFatViewSet(viewsets.ModelViewSet):
    serializer_class = BodyFatSerializer
    queryset = MuscleSize.objects.filter(goal=False).annotate(Year=TruncYear('date'))
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['body_fat_percentage', 'date', 'client', 'goal']