from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.muscle_size import MuscleSize
from gestion_salle.api.serializers import MuscleSizeSerializer


class MuscleSizeViewSet(viewsets.ModelViewSet):
    serializer_class = MuscleSizeSerializer
    queryset = MuscleSize.objects.all()

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['ankle', 'calf', 'thigh', 'waist', 'pectoral', 'shoulder', 'wrist',
                        'forearm', 'arm', 'neck', 'weight', 'body_fat_percentage', 'date', 'client', 'goal']



"""
class LastMuscleViewSet(viewsets.ModelViewSet):
    serializer_class = MuscleSizeSerializer
    queryset = MuscleSize.objects.last()

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['ankle', 'calf', 'thigh', 'waist', 'pectoral', 'shoulder', 'wrist',
                        'forearm', 'arm', 'neck', 'weight', 'date', 'client', 'goal']

    serializer_class = MuscleSizeSerializer
    queryset = MuscleSize.objects.latest('date')

    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['ankle', 'calf', 'thigh', 'waist', 'pectoral', 'shoulder', 'wrist',
                        'forearm', 'arm', 'neck', 'weight', 'date', 'client', 'goal']
"""