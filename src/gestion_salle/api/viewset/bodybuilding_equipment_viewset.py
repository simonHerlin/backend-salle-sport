from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.bodybuilding_equipment import BodyBuildingEquipment
from gestion_salle.api.serializers import BodyBuildingEquipmentSerializer


class BodyBuildingEquipmentViewSet(viewsets.ModelViewSet):
    serializer_class = BodyBuildingEquipmentSerializer

    queryset = BodyBuildingEquipment.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name']
