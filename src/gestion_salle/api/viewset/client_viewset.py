from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.client import Client
from gestion_salle.api.serializers import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer

    queryset = User.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id', 'first_name', 'last_name', 'username', 'email']


#    @action(methods='get', detail=True)
#    def get_queryset(self):
#        return Client.objects.all()

#    def filter_queryset(self, queryset):
#       print(request.query_params)


"""
    @action(methods='get', detail=True)
    def get_queryset(self, pk=None):
        id = self.kwargs['pk']
        return Client.objects.filter(id=id)

    @action(methods='get', detail=True)
    def getByName_queryset(self, name=None):
        name = self.kwargs['name']
        return Client.objects.filter(permission__client__last_name=name)
"""

"""
    @action(methods=['get'], detail=True, url_path='client', url_name='client')
    def get_clients(self):
        self.get_object()
        groups = client.groups.all()
        return Response([group.name for group in groups])

    @action(methods=['get'], detail=True, url_path="client/<int:pk>", url_name='client')
    def get_client(self, pk):
        self.get_object()
        groups = client.Client.
"""
