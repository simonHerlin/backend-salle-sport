from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from gestion_salle.models.performance import Performance
from gestion_salle.api.serializers import PerformanceSerializer


class PerformanceViewSet(viewsets.ModelViewSet):
    serializer_class = PerformanceSerializer

    queryset = Performance.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['client', 'equipment', 'score']
