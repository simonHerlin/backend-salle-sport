from rest_framework import serializers

from gestion_salle.models.food import Food


class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = (
            'name',
            'protein',
            'lipid',
            'carbohydrate',
            'gram'
        )
