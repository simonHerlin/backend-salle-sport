from .client_serializer import ClientSerializer
from .muscle_size_serializer import MuscleSizeSerializer
from .permission_serializer import PermissionSerializer
from .bodybuilding_equipment_serializer import BodyBuildingEquipmentSerializer
from .performance_serializer import PerformanceSerializer
from .city_serializer import CitySerializer
from .postal_code_serializer import PostalCodeSerializer
from .one_muscle_size_serializer import (
    AnkleMuscleSizeSerializer, CalfMuscleSizeSerializer, ArmMuscleSizeSerializer,
    PectoralMuscleSizeSerializer, NeckMuscleSizeSerializer, ForearmMuscleSizeSerializer,
    ShoulderMuscleSizeSerializer, ThighMuscleSizeSerializer, WaistMuscleSizeSerializer,
    WristMuscleSizeSerializer, WeightMuscleSizeSerializer, BodyFatSerializer)
from .food_serializer import FoodSerializer
from .daily_food_serializer import DailyFoodSerializer


[
    ClientSerializer,
    MuscleSizeSerializer,
    PermissionSerializer,
    BodyBuildingEquipmentSerializer,
    PerformanceSerializer,
    CitySerializer,
    PostalCodeSerializer,
    AnkleMuscleSizeSerializer,
    CalfMuscleSizeSerializer,
    ArmMuscleSizeSerializer,
    PectoralMuscleSizeSerializer,
    NeckMuscleSizeSerializer,
    ForearmMuscleSizeSerializer,
    ShoulderMuscleSizeSerializer,
    ThighMuscleSizeSerializer,
    WaistMuscleSizeSerializer,
    WristMuscleSizeSerializer,
    WeightMuscleSizeSerializer,
    BodyFatSerializer,
    FoodSerializer,
    DailyFoodSerializer,
]