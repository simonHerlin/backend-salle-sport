from rest_framework import serializers

from gestion_salle.models.muscle_size import MuscleSize


class MuscleSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MuscleSize
        fields = (
            'ankle',
            'calf',
            'thigh',
            'waist',
            'pectoral',
            'shoulder',
            'wrist',
            'forearm',
            'arm',
            'neck',
            'date',
            'client',
            'weight',
            'body_fat_percentage')
