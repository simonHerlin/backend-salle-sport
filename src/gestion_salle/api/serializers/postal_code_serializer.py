from rest_framework import serializers

from gestion_salle.models.postal_code import PostalCode


class PostalCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostalCode
        fields = ('postal_code', )
