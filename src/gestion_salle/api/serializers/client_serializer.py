from django.contrib.auth.models import User
from rest_framework import serializers
from django import forms
from gestion_salle.models.client import Client


class ClientSerializer(serializers.ModelSerializer):
    date_of_birth = serializers.CharField(source='client.date_of_birth')
    phone = serializers.CharField(source='client.phone')
    code = serializers.CharField(source='client.code')
    permission = serializers.CharField(source='client.permission')
    city = serializers.CharField(source='client.city')

    class Meta:
        model = User
        fields = ('id',
                  'first_name',
                  'last_name',
                  'username',
                  'date_of_birth',
                  'email',
                  'phone',
                  'code',
                  'permission',
                  'city')
