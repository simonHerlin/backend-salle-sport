from rest_framework import serializers

from gestion_salle.models.performance import Performance


class PerformanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Performance
        fields = ('client', 'equipment', 'score')
