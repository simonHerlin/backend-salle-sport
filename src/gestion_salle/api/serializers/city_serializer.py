from rest_framework import serializers

from gestion_salle.models.city import City


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('name', 'postal_code', )
