from rest_framework import serializers

from gestion_salle.models.daily_food import DailyFood


class DailyFoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = DailyFood
        fields = (
            'protein',
            'lipid',
            'carbohydrate',
            'date'
        )
