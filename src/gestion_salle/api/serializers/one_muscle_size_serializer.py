from rest_framework import serializers

from gestion_salle.models.muscle_size import MuscleSize


class AnkleMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='ankle')

    class Meta:
        model = MuscleSize
        fields = ('date', 'value')


class CalfMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='calf')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class ThighMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='thigh')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class WaistMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='waist')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class PectoralMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='pectoral')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class ShoulderMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='shoulder')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class WristMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='wrist')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class ForearmMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='forearm')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class ArmMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='arm')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class NeckMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='neck')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class WeightMuscleSizeSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='weight')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')


class BodyFatSerializer(serializers.ModelSerializer):
    value = serializers.FloatField(source='body_fat_percentage')

    class Meta:
        model = MuscleSize
        fields = ('value', 'date')
