from rest_framework import serializers

from gestion_salle.models.bodybuilding_equipment import BodyBuildingEquipment


class BodyBuildingEquipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = BodyBuildingEquipment
        fields = ('name', )
