from gestion_salle.api.viewset.client_viewset import ClientViewSet
from gestion_salle.api.viewset.permission_viewset import PermissionViewSet
from gestion_salle.api.viewset.muscle_size_viewset import MuscleSizeViewSet
from gestion_salle.api.viewset.bodybuilding_equipment_viewset import BodyBuildingEquipmentViewSet
from gestion_salle.api.viewset.performance_viewset import PerformanceViewSet
from gestion_salle.api.viewset.city_viewset import CityViewSet
from gestion_salle.api.viewset.postal_code_viewset import PostamCodeViewSet
from rest_framework.routers import DefaultRouter
from gestion_salle.api.viewset.one_muscle_size_viewset import (WaistSizeViewSet,
                                                               ThighSizeViewSet,
                                                               ShoulderSizeViewSet,
                                                               PectoralSizeViewSet,
                                                               NeckSizeViewSet,
                                                               AnkleSizeViewSet,
                                                               ArmSizeViewSet,
                                                               CalfSizeViewSet,
                                                               ForearmSizeViewSet,
                                                               WristSizeViewSet,
                                                               WeightViewSet,
                                                               BodyFatViewSet)
from gestion_salle.api.viewset.food_viewset import FoodViewSet


router = DefaultRouter()
router.register('client', ClientViewSet, basename='client')
router.register('muscle_size', MuscleSizeViewSet, basename='muscle_size')
router.register('permission', PermissionViewSet, basename='permission')
router.register('bodybuilding_equipment', BodyBuildingEquipmentViewSet, basename='bodybuilding_equipment')
router.register('performance', PerformanceViewSet, basename='performance')
router.register('city', CityViewSet, basename='city')
router.register('postal_code', PostamCodeViewSet, basename='postal_code')
router.register('ankle', AnkleSizeViewSet, basename='muscle_size')
router.register('arm', ArmSizeViewSet, basename='muscle_size')
router.register('waist', WaistSizeViewSet, basename='muscle_size')
router.register('calf', CalfSizeViewSet, basename='muscle_size')
router.register('thigh', ThighSizeViewSet, basename='muscle_size')
router.register('shoulder', ShoulderSizeViewSet, basename='muscle_size')
router.register('pectoral', PectoralSizeViewSet, basename='muscle_size')
router.register('neck', NeckSizeViewSet, basename='muscle_size')
router.register('forearm', ForearmSizeViewSet, basename='muscle_size')
router.register('wrist', WristSizeViewSet, basename='muscle_size')
router.register('weight', WeightViewSet, basename='muscle_size')
router.register('body_fat_percentage', BodyFatViewSet, basename='muscle_size')
router.register('food', FoodViewSet, basename='food')
urlpatterns = router.urls
