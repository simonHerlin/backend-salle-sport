from django.contrib import admin

from gestion_salle.models.client import Client
from gestion_salle.models.permission import Permission
from gestion_salle.models.muscle_size import MuscleSize
from gestion_salle.models.bodybuilding_equipment import BodyBuildingEquipment
from gestion_salle.models.performance import Performance
from gestion_salle.models.postal_code import PostalCode
from gestion_salle.models.city import City


admin.site.register(Client)
admin.site.register(Permission)
admin.site.register(MuscleSize)
admin.site.register(BodyBuildingEquipment)
admin.site.register(Performance)
admin.site.register(PostalCode)
admin.site.register(City)
